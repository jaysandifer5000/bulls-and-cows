from random import randint

def generate_secret():
    my_list = []
    while len(my_list) < 4:
        n = randint(0,9)
        if n not in my_list:
            my_list.append(n)
    return my_list

def parse_number(s):
    l = []
    for n in s:
        number = int(n)
        l.append(number)
    print ("l: ", l)
    return l

def count_exact_matches(first, second):
    count = 0
    for a, b in zip(first, second):
        if a == b:
            count = count + 1
    return count

def count_common_entries(first, second):
    count = 0
    for entry in first:
        if entry in second:
            count = count + 1
    return count

    
  


def play_game():
    print("Let's play Bulls and Cows!")
    print("--------------------------")
    print("I have four secret numbers.")
    print("Can you guess them?")
    print("You have 20 tries.")
    print()

    secret = generate_secret()
    print("secret: ", secret)
    number_of_guesses = 20

    for i in range(number_of_guesses):
        prompt = "Type in guess #" + str(i + 1) + ": "
        guess = input(prompt)

        while len(guess) != 4:
            print("You must enter a four-digit number")
            guess = input(prompt)

        converted_guess = parse_number(guess)

        total_exact_matches = count_exact_matches(converted_guess, secret)
      
        total_common_entries = count_common_entries(converted_guess, secret)

     

        if total_exact_matches == 4:
            print("You've guessed the right numbers")
            return

     
        print("Total number of bulls: ", total_exact_matches)

      
        print("Total number of cows: ", total_common_entries - total_exact_matches)


    
    print("The secret was: ", secret)


def run():
    response = input("Do you want to play a game? Y or N\n")

    while response == ("Y"):
        play_game()
        response = input("Do you still want to play? Y or N\n")


if __name__ == "__main__":
    run()
